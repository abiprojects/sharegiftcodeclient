﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;

public class StreamVideo : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public Button btPlay;
    public Button btPause;
    public RawImage rawImage;
    public VideoPlayer videoPlayer;
    public VideoSource videoSource;
    public AudioSource audioSource;
    public int quality;
    public bool startVideo = false;
    // Start is called before the first frame update
    void Start()
    {
        //Show("http://techslides.com/demos/sample-videos/small.mp4");
    }

    // Update is called once per frame
    public void Show(string urlVideo)
    {
        StartCoroutine(PlayVideo(urlVideo));
    }
    public IEnumerator PlayVideo(string urlVideo)
    {
        rawImage.color = new Color(1, 1, 1, 0);
        videoPlayer.playOnAwake = false;
        audioSource.playOnAwake = false;
        audioSource.Pause();
        videoPlayer.source = VideoSource.Url;
        videoPlayer.url = urlVideo;
        videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
        videoPlayer.EnableAudioTrack(0, true);
        videoPlayer.SetTargetAudioSource(0, audioSource);
        videoPlayer.Prepare();
        WaitForSeconds waitForSeconds = new WaitForSeconds(1);
        while (!videoPlayer.isPrepared)
        {
            Debug.Log("isPrepareding");
            yield return waitForSeconds;
            break;
        }
        Debug.Log("done isPrepareding video");
        rawImage.texture = videoPlayer.texture;
        rawImage.color = new Color(1, 1, 1, 1);
        //videoPlayer.Play();
        //audioSource.Play();

        videoPlayer.Pause();
        audioSource.Pause();
        startVideo = true;
        btPlay.gameObject.SetActive(true);
        btPause.gameObject.SetActive(false);
        //while (videoPlayer.isPlaying)
        //{
        //    Debug.Log("video time " + Mathf.FloorToInt((float)videoPlayer.time));
        //    yield return null;
        //}
    }
    public void PlauseVideo()
    {
        if (!startVideo) return;
        btPlay.gameObject.SetActive(true);
        btPause.gameObject.SetActive(false);
        if (videoPlayer != null) videoPlayer.Pause();
        if (audioSource != null) audioSource.Pause();
    }
    public void PlayVideo()
    {
        if (!startVideo) return;
        btPlay.gameObject.SetActive(false);
        btPause.gameObject.SetActive(true);
        if (videoPlayer != null) videoPlayer.Play();
        if (audioSource != null) audioSource.Play();
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (!startVideo) return;
        if (videoPlayer.isPlaying)
        {
            btPlay.gameObject.SetActive(false);
            btPause.gameObject.SetActive(true);
        }
        else
        {
            btPlay.gameObject.SetActive(true);
            btPause.gameObject.SetActive(false);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (!startVideo) return;
        btPlay.gameObject.SetActive(false);
        btPause.gameObject.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!startVideo) return;
        if(videoPlayer.isPlaying)
        {
            PlauseVideo();
        }
        else
        {
            PlayVideo();
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EffectUseKey : MonoBehaviour
{
    public Sprite[] Keys;

    public Image key1;
    public Image key2;
    public Image StartKey;
    public Image box;
    public Text messge1;
    public Text messge2;
    public Button btHelp;
    //private Vector3 posKeyStart = new Vector3(108, 128, 0);
    //private Vector3 posKeyEnd1 = new Vector3(-60, -150, 0);
    //private Vector3 posKeyEnd2 = new Vector3(-75, -63, 0);
    Action actionEndEffect;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Show(int numberKeyEffect = 1, int posKey = 1, Action action = null)
    {
        gameObject.SetActive(true);
        actionEndEffect = action;
        StartCoroutine(ZoomWithTime(StartKey.rectTransform, Vector3.one, new Vector3(2, 2, 1), 0.3f, numberKeyEffect, posKey));
    }
    public IEnumerator ZoomWithTime(RectTransform rectTransform, Vector3 scaleStart, Vector3 scaleEnd, float time, int numberKeyEffect = 1, int posKey = 1)
    {

        float t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime / time; // sweeps from 0 to 1 in time seconds
            rectTransform.localScale = Vector3.Lerp(scaleStart, scaleEnd,t); // set position proportional to t
            yield return t; // leave the routine and return here in the next frame
        }
        rectTransform.localScale = scaleEnd;
        t = 0f;
        while (t < 1f)
        {
            t += Time.deltaTime / time; // sweeps from 0 to 1 in time seconds
            rectTransform.localScale = Vector3.Lerp(scaleEnd, scaleStart, t); // set position proportional to t
            yield return t; // leave the routine and return here in the next frame
        }
        rectTransform.localScale = scaleStart;
        key1.gameObject.SetActive(false);
        box.gameObject.SetActive(true);
        if (posKey == 2)
        {
            //box.sprite = boxKey2[0];
            key1.gameObject.SetActive(true);
            key2.gameObject.SetActive(false);
            StartKey.gameObject.SetActive(true);
            StartCoroutine(moveWithTime(StartKey.rectTransform, StartKey.rectTransform.position, key2.rectTransform.position, 1f, ()=> {
                key2.gameObject.SetActive(false);
                StartKey.gameObject.SetActive(false);
                //box.sprite = boxKey2[0];
                key2.gameObject.SetActive(true);
                StartCoroutine(XoayKhoa(Keys,key2, 0.1f, () =>
                {
                    if (actionEndEffect != null)
                    {
                        actionEndEffect();
                    }
                }));
            }));
        }
        else if(posKey == 1 && numberKeyEffect == 1)
        {
            //box.sprite = boxKey1[0];
            key1.gameObject.SetActive(false);
            StartKey.gameObject.SetActive(true);
            StartCoroutine(moveWithTime(StartKey.rectTransform, StartKey.rectTransform.position, key1.rectTransform.position, 1f,()=> {
                StartKey.gameObject.SetActive(false);
                key1.gameObject.SetActive(true);
                key2.gameObject.SetActive(false);
                //box.sprite = boxKey1[0];
                StartCoroutine(XoayKhoa(Keys, key1, 0.1f, () =>
                {
                    StartCoroutine(moveLocaltionWithTime(box.rectTransform, box.rectTransform.localPosition, box.rectTransform.localPosition + new Vector3(0, 400, 0), 1f, () =>
                    {
                        messge1.gameObject.SetActive(true);
                        messge2.gameObject.SetActive(true);
                        RectTransform btHelpRect = btHelp.GetComponent<RectTransform>();
                        StartCoroutine(moveLocaltionWithTime(btHelpRect, btHelpRect.localPosition, btHelpRect.localPosition + new Vector3(0, 400, 0), 1f));
                    }));
                }));
            }));
        }
        else if (posKey == 1 && numberKeyEffect == 2)
        {
            //box.sprite = boxKey1[0];
            key1.gameObject.SetActive(false);
            key2.gameObject.SetActive(false);
            StartCoroutine(moveWithTime(StartKey.rectTransform, StartKey.rectTransform.position, key1.rectTransform.position, 1f, () =>
            {
                key1.gameObject.SetActive(true);
                key2.gameObject.SetActive(false);
                //box.sprite = boxKey1[0];
                StartCoroutine(XoayKhoa(Keys,key1, 0.1f, () =>
                {
                    key2.gameObject.SetActive(true);
                    StartCoroutine(XoayKhoa(Keys, key2, 0.1f, () =>
                    {
                        if (actionEndEffect != null)
                        {
                            actionEndEffect();
                        }
                    }));
                }));
            }));
        }
    }
    public IEnumerator moveWithTime(RectTransform rectTransform, Vector3 posStart, Vector3 posEnd, float time, Action action = null)
    {

        float t = 0f;

        while (t < 1f)
        {
            t += Time.deltaTime / time; // sweeps from 0 to 1 in time seconds
            rectTransform.position = Vector3.Lerp(posStart, posEnd, t); // set position proportional to t
            yield return t; // leave the routine and return here in the next frame
        }
        rectTransform.position = posEnd;
        if (action != null)
        {
            action();
        }
    }
    public IEnumerator moveLocaltionWithTime(RectTransform rectTransform, Vector3 posStart, Vector3 posEnd, float time, Action action = null)
    {

        float t = 0f;

        while (t < 1f)
        {
            t += Time.deltaTime / time; // sweeps from 0 to 1 in time seconds
            rectTransform.localPosition = Vector3.Lerp(posStart, posEnd, t); // set position proportional to t
            yield return t; // leave the routine and return here in the next frame
        }
        rectTransform.localPosition = posEnd;
        if (action != null)
        {
            action();
        }
    }
    public IEnumerator XoayKhoa(Sprite[]sprites,Image image, float speed, Action action = null)
    {
        int countsprite = 0;
        while (countsprite < sprites.Length)
        {
            yield return new WaitForSeconds(speed);
            image.sprite = sprites[countsprite];
            countsprite += 1;
        }
        if (action != null)
        {
            action();
        }
    }
    public void HelpClick()
    {
        AppController.instance.popupShare.Show();
    }
}

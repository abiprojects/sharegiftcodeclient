﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestLogin : MonoBehaviour
{
    public InputField inputUserId;
    public InputField inputUserName;
    public InputField inputShareLinkId;
    public Text err;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Show()
    {
        err.text = "";
        gameObject.SetActive(true);
    }
    public void btLoginClick()
    {
        string userId = inputUserId.text.Trim();
        string userName = inputUserName.text.Trim();
        string shareLinkId = inputShareLinkId.text.Trim();
        if(userId == "" || userName == "")
        {
            err.text = "userId và userName phải khác rỗng";
            return;
        }
        StartCoroutine(NetWorkManager.LoginFB("/users/", userId, userName, shareLinkId, LoginType.FB, AppController.instance.LoginSuccess));
    }
    public void btClose()
    {
        gameObject.SetActive(false);
    }
}

﻿using Facebook.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public enum LoginType { FB,Line,Kakao}
public class AppController : MonoBehaviour
{
    public static AppController instance;
    public static bool isTest = true;
    public bool isLogin = false;
    
    public GameObject formNormal;
    public GameObject key1;
    public GameObject key2;
    public TestLogin testLogin;
    public PopUpLogin popUpLogin;
    public PopupShare popupShare;
    public EffectUseKey effectUseKey;
    public PopupContenHelp popupContenHelp;
    public FormShowGiftCode formShowGiftCode;
    public StreamVideo streamVideo;
    public Image iconBox;
    public Text contentInfo;
    public Text numberKey;
    public Toggle statusIsTest;
    public DataLoginSuccess dataLoginSuccess;
    public static string SharelinkId = "";
    public static string LinkGG = "https://play.google.com/store/apps/details?id=com.alien.shooter.galaxy.attack";
    public static string LinkApple = "https://apps.apple.com/US/app/id1176011642?mt=8";
    public static string linkVideoLogin;
    public static string linkVideoEvent;
    public static string linkShareEvent;
    public static string linkFanpageFB = "https://www.facebook.com/GalaxyAttack/";
    private void Awake()
    {
        instance = this;
        if(Application.absoluteURL != "")
        {
            Uri myUri = new Uri(Application.absoluteURL);
            
            SharelinkId = System.Web.HttpUtility.ParseQueryString(myUri.Query).Get("id");
            if (SharelinkId == null) SharelinkId = "";
            Debug.Log("SharelinkId = " + SharelinkId);
            Debug.Log("AbsoluteUri = " + myUri.AbsoluteUri);
            Debug.Log("Host = " + myUri.Host);
            Debug.Log("AbsolutePath = " + myUri.AbsolutePath);
            Debug.Log("IdnHost = " + myUri.IdnHost);
            Debug.Log("LocalPath = " + myUri.LocalPath);
            Debug.Log("OriginalString = " + myUri.OriginalString);
            Debug.Log("DnsSafeHost = " + myUri.DnsSafeHost);
            Debug.Log("Fragment = " + myUri.Fragment);
            Debug.Log("OriginalString = " + myUri.OriginalString);
            Debug.Log("UserInfo = " + myUri.UserInfo);
            Debug.Log("Port = " + myUri.Port);
        }
        
    }
    // Start is called before the first frame update
    void Start()
    {
        contentInfo.text = "Bạn chưa có chìa khóa nào, đăng nhập để nhận khóa";// "Bạn đã có 1 chìa khóa rồi";
        //iconBox.sprite = ResouceController.instance.IconsBoxNoKey;
        key1.SetActive(false);
        key2.SetActive(false);
        numberKey.text = "";
        statusIsTest.isOn = isTest;
        if (isLogin)
        {
            HideLogin();
        }
        else
        {
            ShowLogin();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ShowHelp()
    {
        popupContenHelp.Show();
    }
    public void UseKey()
    {
        if (!isLogin || dataLoginSuccess == null)
        {
            ShowLogin();
            return;
        }
        StartCoroutine(NetWorkManager.UserKey("/users/UseKey", dataLoginSuccess.UserId, AppController.instance.UseKeySuccess));
    }
    public void InviteOther()
    {
        if (!isLogin || dataLoginSuccess == null)
        {
            ShowLogin();
            return;
        }
        popupShare.Show();
    }
    public void LoginSuccess(string data)
    {
        HideLogin();
        DataLoginRespon dataLoginRespon = null;
        try
        {
            //dataLoginRespon = JsonUtility.FromJson<DataLoginRespon>(data);
            dataLoginRespon = Newtonsoft.Json.JsonConvert.DeserializeObject<DataLoginRespon>(data);
            Debug.Log("dataLoginRespon " + dataLoginRespon.ToString());
        }
        catch(Exception e)
        {
            Debug.Log("LoginSuccess Exception" + e.ToString());
        }
        
        if (dataLoginRespon != null)
        {
            popUpLogin.Hide();
            isLogin = true;
            dataLoginSuccess = dataLoginRespon.data;
            LinkGG = dataLoginRespon.LinkGG;
            LinkApple = dataLoginRespon.linkApple;
            linkVideoLogin = dataLoginRespon.linkVideoLogin;
            linkVideoEvent = dataLoginRespon.linkVideoEvent;
            linkShareEvent = dataLoginRespon.LinkShareEvent;
            linkFanpageFB = dataLoginRespon.LinkFanpageFB;
            if (dataLoginSuccess != null)
            {
                if (dataLoginSuccess.GiftCode != "")//hiển thị thông tin nhận thưởng
                {
                    if (dataLoginRespon.data.KeysUse >= 2)
                    {
                        formNormal.SetActive(false);
                        formShowGiftCode.Show(linkVideoEvent, LinkGG, LinkApple, dataLoginRespon.Title1, dataLoginRespon.Title2, dataLoginRespon.UrlItemGiftCode, dataLoginSuccess.GiftCode, dataLoginRespon.TermsAndConditions, linkShareEvent, linkFanpageFB);
                    }
                    else
                    {
                        if (linkVideoLogin != "") streamVideo.Show(linkVideoLogin);
                        if (dataLoginSuccess.KeysFree > 0) numberKey.text = "X" + dataLoginSuccess.KeysFree;

                        if (dataLoginSuccess.KeysUse != 0)
                        {
                            key1.SetActive(true);
                            key2.SetActive(false);
                        }
                        if (dataLoginSuccess.KeysFree > 0)
                        {
                            contentInfo.text = "Bạn đang có " + dataLoginSuccess.KeysFree + " chìa khóa, hãy sử dụng chúng để nhận quà";
                        }
                        else
                        {
                            contentInfo.text = "Hãy nhờ bạn bè trợ giúp để nhận thêm khóa";
                        }
                    }
                }
                else
                {
                    if (linkVideoLogin != "") streamVideo.Show(linkVideoLogin);
                    if(dataLoginSuccess.KeysFree >0) numberKey.text = "X"+ dataLoginSuccess.KeysFree;
                    
                    if(dataLoginSuccess.KeysUse != 0)
                    {
                        key1.SetActive(true);
                        key2.SetActive(false);
                    }
                    if (dataLoginSuccess.KeysFree > 0)
                    {
                        contentInfo.text = "Bạn đang có " + dataLoginSuccess.KeysFree + " chìa khóa, hãy sử dụng chúng để nhận quà";
                    }
                    else
                    {
                        contentInfo.text = "Hãy nhờ bạn bè trợ giúp để nhận thêm khóa";
                    }
                }
            }
            
        }
        else
        {
            Debug.Log("dataLoginRespon == null || dataLoginRespon.data == null");
        }
    }
    public void UseKeySuccess(string data)
    {
        try
        {
            Debug.Log("UseKeySuccess " + data);
            DataUesKeyRespon dataUesKeyRespon = Newtonsoft.Json.JsonConvert.DeserializeObject<DataUesKeyRespon>(data);
            Debug.Log("dataUesKeyRespon " + dataUesKeyRespon.ToString());
            if (dataUesKeyRespon != null)
            {
                Debug.Log("dataUesKeyRespon.data " + dataUesKeyRespon.data.ToString());
                if (dataUesKeyRespon.status)
                {
                    if(dataUesKeyRespon.data != null)
                    {
                        int keyfree = dataUesKeyRespon.data.KeyFree;
                        int keyused = dataUesKeyRespon.data.KeyUsed;
                        if (keyfree > 2) keyfree = 2;
                        if (keyused > 2) keyused = 2;
                        if (keyfree < 0) keyfree = 0;
                        if (keyused < 0) keyused = 0;
                        if (keyfree > 0)
                        {
                            int numberUseKey = keyfree;
                            int posKey = 1;
                            if (keyused > 0) posKey = 2;
                            formNormal.SetActive(false);
                            effectUseKey.Show(numberUseKey, posKey,()=> {
                                effectUseKey.gameObject.SetActive(false);
                                string giftcode = dataUesKeyRespon.data.GiftCode;
                                if (giftcode == "") return;
                                formShowGiftCode.Show(linkVideoEvent, LinkGG, LinkApple, dataUesKeyRespon.data.Title1, dataUesKeyRespon.data.Title2, dataUesKeyRespon.data.UrlItemGiftCode,giftcode, dataUesKeyRespon.data.TermsAndConditions,linkShareEvent , linkFanpageFB);
                            });
                        }
                    }
                }
            }
            else
            {
                Debug.Log("dataUesKeyRespon == null");
            }
        }
        catch (Exception e)
        {
            Debug.Log("LoginSuccess Exception" + e.ToString());
        }

        
    }
    public void OpenLinkGG()
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            OpenLinkJSPlugin(LinkGG);
            return;
        }
        if (LinkGG !="") Application.OpenURL(LinkGG);
    }
    public void OpenLinkApple()
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            OpenLinkJSPlugin(LinkApple);
            return;
        }
        if (LinkApple != "") Application.OpenURL(LinkApple);
    }
    public void OpenLinkFanpageFB()
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            OpenLinkJSPlugin(linkFanpageFB);
            return;
        }
        if (linkFanpageFB != "") Application.OpenURL(linkFanpageFB);
    }
    public void ShowLogin()
    {
        if (isTest)
        {
            testLogin.Show();
        }
        else
        {
            popUpLogin.Show();
        }
    }
    public void HideLogin()
    {
        testLogin.btClose();
        popUpLogin.Hide();
    }
    public void ChangeStatusTest()
    {
        isTest = statusIsTest.isOn;
    }
    public void OpenLinkJSPlugin(string url)
    {
#if !UNITY_EDITOR
		openWindow(url);
#endif
    }

    [DllImport("__Internal")]
    private static extern void openWindow(string url);
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetWorkManager : MonoBehaviour
{
    //public static string host = "http://localhost:3000";
    public static string host = "https://sandbox.abigames.com.vn/nodejs";
    public static IEnumerator LoginFB(string url, string id, string name, string sharelinkId, LoginType loginType, Action<string> action)
    {
        url = host + url;
        Debug.Log("LoginFB url = " + url);
        WWWForm form = new WWWForm();
        form.AddField("name", name);
        Debug.Log("LoginFB form = " + Newtonsoft.Json.JsonConvert.SerializeObject(form));
        form.AddField("id", id);
        Debug.Log("LoginFB form = " + Newtonsoft.Json.JsonConvert.SerializeObject(form));
        form.AddField("sharelinkId", sharelinkId);
        Debug.Log("LoginFB form = " + Newtonsoft.Json.JsonConvert.SerializeObject(form));
        form.AddField("login_type", (int)loginType);
        Debug.Log("LoginFB form = " + Newtonsoft.Json.JsonConvert.SerializeObject(form));
        UnityWebRequest getRequest = UnityWebRequest.Post(url, form);
        yield return getRequest.SendWebRequest();
        if (getRequest.isNetworkError)
        {
            Debug.Log(getRequest.error);
            yield return null;
        }
        else
        {
            string data = getRequest.downloadHandler.text;
            Debug.Log(data);
            if (action != null) action(data);
        }
    }
    public static IEnumerator UserKey(string url, string id, Action<string> action)
    {
        url = host + url;
        WWWForm form = new WWWForm();
        form.AddField("id", id);
        UnityWebRequest getRequest = UnityWebRequest.Post(url, form);
        yield return getRequest.SendWebRequest();
        if (getRequest.isNetworkError)
        {
            Debug.Log(getRequest.error);
        }
        else
        {
            string data = getRequest.downloadHandler.text;
            Debug.Log(data);
            if (action != null) action(data);
        }
    }
    public static IEnumerator ShareLink(string url, string UserShare_Id, string UserShareId, Action<string> action)
    {
        url = host + url;
        WWWForm form = new WWWForm();
        form.AddField("UserShare_Id", UserShare_Id);
        form.AddField("UserShareId", UserShareId);
        UnityWebRequest getRequest = UnityWebRequest.Post(url, form);
        yield return getRequest.SendWebRequest();
        if (getRequest.isNetworkError)
        {
            Debug.Log(getRequest.error);
        }
        else
        {
            string data = getRequest.downloadHandler.text;
            Debug.Log(data);
            if (action != null) action(data);
        }
    }
}

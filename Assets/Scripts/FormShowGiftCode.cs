﻿using Facebook.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class FormShowGiftCode : MonoBehaviour
{
    public StreamVideo streamVideo;
    public Text title1;
    public Text title2;
    public Text giftcode;
    public Text termsAndConditions;
    public GameObject objShare;
    public Image EffectBox;

    public string linkgg,linkapple, linkEvent,linkFanpageFb;

    [DllImport("__Internal")]
    private static extern void CopyToClipBoard(string str);
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        EffectBox.rectTransform.Rotate(Vector3.forward, 360 * Time.deltaTime);
    }
    public void Show(string linkVideo, string _linkgg, string _linkapple,string _title1, string _title2, string _linkitem,string _giftcode,string _termsAndConditions, string _linkEvent,string _linkFanpageFB)
    {
        gameObject.SetActive(true);
        streamVideo.Show(linkVideo);
        linkgg = _linkgg;
        linkapple = _linkapple;
        title1.text = _title1;
        if(title2 != null) title2.text = _title2;
        giftcode.text = _giftcode;
        termsAndConditions.text = _termsAndConditions;
        linkEvent = _linkEvent;
        linkFanpageFb = _linkFanpageFB;
        
    }
    
    public void OpenLinkGG()
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            AppController.instance.OpenLinkJSPlugin(linkgg);
            return;
        }
        Application.OpenURL(linkgg);
    }
    public void OpenLinkApple()
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            AppController.instance.OpenLinkJSPlugin(linkapple);
            return;
        }
        Application.OpenURL(linkapple);
    }
    public void Share()
    {
        objShare.SetActive(true);
    }
    public void ShareFB()
    {
        Uri uri = new Uri(linkEvent);
        string contentTitle = "test";
        string conten = "dsfsafasfsdfsdfasfdsaf";
        FB.ShareLink(uri, contentTitle, conten, null, null);
    }
    public void ShareTwiter()
    {

    }
    public void ShareLine()
    {

    }
    public void ShareKakao()
    {

    }
    public void OpenFanpageFB()
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            AppController.instance.OpenLinkJSPlugin(linkFanpageFb);
            return;
        }
        Application.OpenURL(linkFanpageFb);
    }
    public void CloseShare()
    {
        objShare.SetActive(false);
    }
    public void btCopyGiftCode()
    {

#if !UNITY_WEBGL
        GUIUtility.systemCopyBuffer = giftcode.text;
#else
        Debug.Log("CopyToClipBoard " + giftcode.text);
        CopyToClipBoard(giftcode.text);
#endif
    }
}

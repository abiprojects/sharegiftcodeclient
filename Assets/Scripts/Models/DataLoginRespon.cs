﻿public class DataLoginRespon 
{
    public bool status;
    public string LinkGG;
    public string linkApple;
    public string linkVideoLogin;
    public string linkVideoEvent;

    public string UrlItemGiftCode;
    public string LinkShareEvent;
    public string LinkFanpageFB;
    public string Title1;
    public string Title2;
    public string TermsAndConditions;

    public DataLoginSuccess data; 
}
public class DataLoginSuccess
{
    public string _id;
    public string UserName;
    public string UserId;
    public string GiftCode;
    public int KeysFree;
    public int KeysUse;
}

﻿public class DataUesKeyRespon
{
    public bool status;
    public DataUesKeySuccess data;
}
public class DataUesKeySuccess
{
    public int KeyFree;
    public int KeyUsed;
    public string UrlItemGiftCode;
    public string GiftCode;
    public string Title1;
    public string Title2;
    public string TermsAndConditions;
}

﻿using Facebook.Unity;
using UnityEngine;
using UnityEngine.UI;

public class FbController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        FB.Init(onInitComplete, onHideUnity);
    }

    private void onHideUnity(bool isUnityShown)
    {
        if (isUnityShown)
        {
            Time.timeScale = 1;
        }
        else
        {
            Time.timeScale = 0;
        }
    }

    private void onInitComplete()
    {
        if (FB.IsLoggedIn)
        {
            Debug.Log("FB.IsLoggedIn = " + FB.IsLoggedIn);
        }
        else
        {
            Debug.Log("FB.IsLoggedIn = " + FB.IsLoggedIn);
        }
    }
}

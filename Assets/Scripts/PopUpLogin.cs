﻿using Facebook.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpLogin : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void LoginFB()
    {
        var permissions = new List<string>();
        permissions.Add("public_profile");
        FB.LogInWithReadPermissions(permissions, AuthLoginCallback);
    }
    private void AuthLoginCallback(ILoginResult result)
    {
        if (result.Error != null)
        {
            Debug.Log("FB.AuthLoginCallback  Error " + result.Error);
            return;
        }
        Debug.Log("FB.AuthLoginCallback  success ");
        FB.API("/me", HttpMethod.GET, GetUserInfoCallback);
    }

    private void GetUserInfoCallback(IGraphResult result)
    {
        if (result.Error != null)
        {
            Debug.Log("GetUserInfoCallback Error " + result.Error);
            return;
        }
        Debug.Log("result.ResultDictionary name" + result.ResultDictionary["name"].ToString());
        Debug.Log("AppController.SharelinkId " + AppController.SharelinkId);
        Debug.Log("GetUserInfoCallback id " + result.ResultDictionary["id"].ToString());
        StartCoroutine(NetWorkManager.LoginFB("/users/", result.ResultDictionary["id"].ToString(), result.ResultDictionary["name"].ToString(), AppController.SharelinkId, LoginType.FB,AppController.instance.LoginSuccess));
    }
    public void LoginLine()
    {

    }
    public void LoginKakao()
    {

    }
    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
}

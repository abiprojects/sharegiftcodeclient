﻿using Facebook.Unity;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupShare : MonoBehaviour
{
    public void ShareFB()
    {
        StartCoroutine(NetWorkManager.ShareLink("/users/sharelink", AppController.instance.dataLoginSuccess._id, AppController.instance.dataLoginSuccess.UserId, ShareFBCallbackApi));
    }
    public void ShareFBCallbackApi(string data)
    {
        DataShareLink dataShareLink = JsonConvert.DeserializeObject<DataShareLink>(data);
        if(dataShareLink.status && dataShareLink.data != "")
        {
            Debug.Log("dataShareLink.data = " + dataShareLink.data);
            Uri uri = new Uri(NetWorkManager.host+"/?id=" + dataShareLink.data);
            string contentTitle = "test";
            string conten = "dsfsafasfsdfsdfasfdsaf";
            FB.ShareLink(uri, contentTitle, conten, null, ShareCallback);
        }
        
    }

    private void ShareCallback(IShareResult result)
    {
        Debug.Log("result = " + result.ToString());
        AppController.instance.contentInfo.text = result.ToString();
    }
    
    public void Show()
    {
        gameObject.SetActive(true);
    }
    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
